package com.runemate.bot.test;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.projection.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.shapes.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.listeners.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import com.runemate.ui.setting.annotation.open.Range;
import com.runemate.ui.setting.annotation.open.*;
import java.awt.*;
import java.util.List;
import java.util.concurrent.*;
import javax.swing.Timer;
import javax.swing.*;
import lombok.*;
import lombok.extern.log4j.*;
import org.apache.commons.lang3.*;

/**
 * This bot can be used to test new features that you're working on.Recommend that you use this to make it obvious to the reviewer
 * how exactly the code change was tested.
 * <p>
 * The "testJar" gradle task will produce a jar containing this bot which you can place in a bot directory.
 * <p>
 * The following gradle command will build this bot and then launch the client:
 * testJar launch --args="--dev"
 */
@Log4j2
public class FeatureTestBot extends LoopingBot implements GameObjectListener {

    @SettingsProvider(updatable = true)
    private ExampleSettings settings;

    JFrame frame;
    Overlay overlay;

    @Override
    public void onStart(final String... arguments) {

//        frame = new JFrame("Model viewer");
//        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        frame.setLocation(Screen.getLocation());
//        frame.setBounds(Screen.getBounds());
//        frame.setAlwaysOnTop(true);
//
//        overlay = new Overlay();
//        frame.add(overlay);
//        frame.setUndecorated(true);
//        frame.setBackground(new Color(0, 0, 0, 0));
//
//        frame.pack();
//        frame.setVisible(true);
    }

    @Override
    public void onLoop() {

    }

    @Override
    public void onGameObjectSpawned(final GameObjectSpawnEvent event) {

    }

    @Override
    public void onStop(final String reason) {
//        overlay.timer.stop();
//        frame.setVisible(false);
//        frame.dispose();
    }

    public class Overlay extends JComponent {

        private final javax.swing.Timer timer;

        public Overlay() {
            timer = new Timer(100, e -> {
                // Code to update the overlay every 50 ms goes here
                try {
                    getPlatform().invokeAndWait(() -> {
                        frame.setLocation(Screen.getLocation());
                        frame.setSize(Screen.getBounds().getSize());
                    });
                } catch (ExecutionException | InterruptedException ignored) {

                }
                repaint();
            });
            timer.start(); // Start the timer
        }

        @SneakyThrows
        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            // Code to render the overlay goes here
            var graphics = (Graphics2D) g;
            graphics.setPaint(Color.RED);
            graphics.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
            getPlatform().invokeAndWait(() -> {

                final var player = Players.getLocal();
                if (player != null) {
                    player.getServerPosition().render(graphics);
                }

                final var object = Npcs.newQuery().results().nearest();
                if (object != null) {
                    graphics.setPaint(Color.BLUE);
                    var clickbox = OpenHull.lookup(((Entity) object).uid);
                    if (clickbox != null) {
                        graphics.draw(clickbox);
                    }

                    var model = object.getModel();
                    if (model != null) {
                        graphics.setPaint(Color.GREEN);
                        model.render(graphics);
                    }
                }

                final var mouse = Mouse.getPosition();
                graphics.drawRect((int) mouse.getX(), (int) mouse.getY(), 2, 2);
            });
        }


    }
}
