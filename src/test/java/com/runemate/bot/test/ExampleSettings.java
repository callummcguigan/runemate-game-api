package com.runemate.bot.test;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.ui.setting.annotation.open.*;
import com.runemate.ui.setting.open.*;

@SettingsGroup(group = "group1")
public interface ExampleSettings extends Settings {

    @SettingsSection(title = "Section title", description = "Section description", order = 0)
    String customSection = "customSection";

    @Setting(key = "testCheckbox", title = "Test Checkbox")
    default boolean testCheckbox() {
        return true;
    }

    @Suffix(Suffix.PERCENT)
    @Range(min = 1, max = 10)
    @Setting(key = "testIntSpinner", title = "Test Integer")
    default int testIntSpinner() {
        return 1;
    }

    @Setting(key = "testIntSpinner", title = "Test Integer")
    void setIntSpinner(int value);

    @Setting(key = "testDoubleSpinner", title = "Test Double")
    default double testDoubleSpinner() {
        return 1.15;
    }

    @Setting(key = "testText", title = "Test TextField")
    default String testText() {
        return "Default Text";
    }

    @Setting(key = "testPassword", title = "Test Password", secret = true)
    default String testPassword() {
        return "Default Text";
    }

    @Setting(key = "playerPosition", title = "Player Position", converter = CoordinateSettingConverter.class, section = customSection)
    default Coordinate testCoordinate() {
        return null;
    }

    @Setting(key = "testState", title = "Test Enum", section = customSection, hidden = true)
    default AbstractBot.State testEnum() {
        return AbstractBot.State.RESTARTING;
    }

    @Setting(key = "testEquipment", title = "Test Equipment", section = customSection)
    default EquipmentLoadout loadout() {
        return new EquipmentLoadout();
    }

}
