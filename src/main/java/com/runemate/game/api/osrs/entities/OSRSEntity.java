package com.runemate.game.api.osrs.entities;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.projection.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.calculations.*;
import com.runemate.game.api.hybrid.util.shapes.*;
import java.awt.*;
import javax.annotation.*;
import lombok.extern.log4j.*;

@Log4j2
public abstract class OSRSEntity extends OSRSCacheModelEntity {

    protected Model cacheModel;

    OSRSEntity(long uid) {
        super(uid);
    }

    @Nullable
    @Override
    public Area.Rectangular getArea(Coordinate regionBase) {
        final Coordinate position = getPosition(regionBase);
        return position != null ? position.getArea() : null;
    }

    @Nullable
    @Override
    public InteractablePoint getInteractionPoint(Point origin) {
        var real = getRealModel();
        if (real != null) {
            final var area = new ViewportArea(real).getVisibleArea();
            log.trace("Determining interaction point of {} using 'real' model", this);
            final var bounds = area.getBounds2D();
            for (int i = 0; i < 50; i++) { //maximum of 50 attempts before falling back to legacy point selection
                var point = new InteractablePoint(
                    (int) Random.nextGaussian(bounds.getMinX(), bounds.getMaxX(), bounds.getMinX() + (bounds.getWidth() / 2)),
                    (int) Random.nextGaussian(bounds.getMinY(), bounds.getMaxY(), bounds.getMinY() + (bounds.getHeight() / 2))
                );
                if (area.contains(point)) {
                    log.trace("Selecting point {}", point);
                    return point;
                }
            }
        }
        log.trace("Falling back to legacy point selection");
        return super.getInteractionPoint(origin);
    }

    @Override
    public boolean isHovered() {
        if (this instanceof Player || this instanceof Npc || this instanceof GameObject || this instanceof GroundItem) {
            return Region.getHoveredEntities().contains(this);
        }
        return super.isHovered();
    }

    @Override
    public boolean contains(Point point) {
        final var real = getRealModel();
        if (real != null) {
            return real.contains(point);
        }
        return super.contains(point);
    }

    @Override
    public final boolean isVisible() {
        if (!isValid()) {
            return false;
        }

        final var real = getRealModel();
        if (real != null) {
            final var area = new ViewportArea(real);
            return area.getVisibility(Projection.getViewport()) > 0.0;
        }

        return super.isVisible();
    }

    @Override
    public final double getVisibility() {
        if (!isValid()) {
            return 0;
        }

        final var real = getRealModel();
        if (real != null) {
            final var area = new ViewportArea(real);
            return area.getVisibility(Projection.getViewport());
        }
        return super.getVisibility();
    }

    private Shape getRealModel() {
        return OpenHull.lookup(uid);
    }

    @Override
    public String toString() {
        return "OSRSEntity";
    }
}
