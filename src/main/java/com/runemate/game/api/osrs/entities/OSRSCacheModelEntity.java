package com.runemate.game.api.osrs.entities;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.hud.Menu;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import java.awt.*;
import java.util.regex.*;
import javax.annotation.*;

public abstract class OSRSCacheModelEntity extends Entity {

    protected Model cacheModel;

    OSRSCacheModelEntity(long uid) {
        super(uid);
    }

    @Nullable
    @Override
    public Area.Rectangular getArea(Coordinate regionBase) {
        final Coordinate position = getPosition(regionBase);
        return position != null ? position.getArea() : null;
    }

    @Nullable
    @Override
    public InteractablePoint getInteractionPoint(Point origin) {
        Interactable interactable = getModel();
        return interactable != null
            ? interactable.getInteractionPoint(origin)
            : (interactable = getArea()) != null ? interactable.getInteractionPoint(origin) : null;
    }

    @Override
    public boolean isHovered() {
        if (this instanceof Player || this instanceof Npc || this instanceof GameObject || this instanceof GroundItem) {
            return Region.getHoveredEntities().contains(this);
        }
        return super.isHovered();
    }

    @Override
    public boolean contains(Point point) {
        Interactable model = getModel();
        return model != null ? model.contains(point) : (model = getArea()) != null && model.contains(point);
    }

    @Override
    public final boolean click() {
        return Mouse.click(this, Mouse.Button.LEFT);
    }

    @Override
    public final boolean interact(final Pattern action, final Pattern target) {
        return Menu.click(this, action, target);
    }

    @Override
    public boolean isVisible() {
        if (!isValid()) {
            return false;
        }

        Interactable model = getModel();
        return model != null ? model.isVisible() : (model = getArea()) != null && model.isVisible();
    }

    @Override
    public double getVisibility() {
        if (!isValid()) {
            return 0;
        }

        Interactable model = getModel();
        return model != null ? model.getVisibility() : (model = getArea()) != null ? model.getVisibility() : 0;
    }

}
