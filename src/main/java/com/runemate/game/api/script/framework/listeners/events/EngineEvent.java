package com.runemate.game.api.script.framework.listeners.events;

import lombok.*;

@Value
public class EngineEvent implements Event {

    Type type;

    public enum Type {
        CLIENT_CYCLE,
        SERVER_TICK
    }
}
