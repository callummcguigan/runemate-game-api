package com.runemate.game.api.script.framework.listeners;

import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;

public interface ProjectileListener extends EventListener {
    void onProjectileLaunched(ProjectileLaunchEvent event);
    void onProjectileMoved(ProjectileMovedEvent event);
}
