package com.runemate.game.api.hybrid.local;

import static com.runemate.game.api.hybrid.local.Varbit.Domain.*;

import com.runemate.game.api.hybrid.util.calculations.*;
import com.runemate.game.api.hybrid.util.collections.*;
import javax.annotation.*;
import lombok.extern.log4j.*;

@Log4j2
public class Varbit {

    private final int varId;
    private final Domain varDomain;
    private final int id, most_significant_bit, least_significant_bit;

    public Varbit(
        int id, int varType, int varId, int most_significant_bit, int least_significant_bit
    ) {
        this.id = id;
        this.varDomain = getDomain(varType);
        this.varId = varId;
        this.most_significant_bit = most_significant_bit;
        this.least_significant_bit = least_significant_bit;
    }

    @Nonnull
    private static Domain getDomain(int varDomain) {
        switch (varDomain) {
            case 0:
                return Domain.PLAYER;
            case 1:
                return Domain.NPC;
            case 2:
                return Domain.CLIENT;
            case 3:
                return Domain.WORLD;
            case 4:
                return Domain.REGION;
            case 5:
                return Domain.OBJECT;
            case 6:
                return Domain.CLAN;
            case 7:
                return Domain.CLAN_SETTING;
            case 8:
                return Domain.DOMAIN_8;
            case 9:
                return Domain.PLAYER_GROUP;
            case 10:
                return Domain.DOMAIN_10;
            default:
                log.warn("Unknown VarDomain for id {}", varDomain);
                return UNKNOWN;
        }
    }

    public int getId() {
        return id;
    }

    public int getVarId() {
        return varId;
    }

    @Nonnull
    public Domain getVarDomain() {
        return varDomain;
    }

    public int getValue() {
        if (Domain.PLAYER.equals(varDomain)) {
            return Varps.getAt(varId).getValueOfBitRange(least_significant_bit, most_significant_bit);
        }
        return -1;
    }

    public int getValue(int varValue) {
        return CommonMath.getBitRange(varValue, least_significant_bit, most_significant_bit);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + most_significant_bit;
        result = 31 * result + least_significant_bit;
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Varbit)) {
            return false;
        }
        Varbit varbit = (Varbit) o;
        return id == varbit.id
            && varDomain == varbit.varDomain
            && most_significant_bit == varbit.most_significant_bit
            && least_significant_bit == varbit.least_significant_bit;

    }

    @Override
    public String toString() {
        return "Varbit{id=" + id + ", domain=" + getVarDomain().name() + '}';
    }

    @Deprecated
    public int getSourceIndex() {
        return varId;
    }

    @Deprecated
    public Varp getSource() {
        return Varps.getAt(varId);
    }

    @Deprecated
    public Pair<Integer, Integer> getSourceExtractionRange() {
        return new Pair<>(least_significant_bit, most_significant_bit);
    }

    public enum Domain {
        PLAYER, PLAYER_GROUP, NPC, CLIENT, WORLD, REGION, OBJECT, CLAN, CLAN_SETTING, DOMAIN_8, DOMAIN_10, UNKNOWN
    }
}
