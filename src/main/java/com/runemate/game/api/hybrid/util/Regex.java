package com.runemate.game.api.hybrid.util;

import static java.util.concurrent.TimeUnit.*;

import com.google.common.cache.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;
import lombok.experimental.*;

@UtilityClass
public final class Regex {

    private final String EQ = "=>";
    private final String CONTAINS = "*>";
    private final String ENDS = "$>";
    private final String STARTS = "^>";
    private final String MULTI = "+>";

    //Backslash must be escaped first, otherwise it will escape the other special characters' backslashes
    private final char[] SPECIAL = { '\\', '(', ')', '^', '$', '.', '*', '?', '|', '[', ']', '{', '}', '+' };

    private final Cache<String, Pattern> CACHE = CacheBuilder.newBuilder().softValues().expireAfterAccess(10, MINUTES).build();
    private final Pattern NULL_PATTERN = Pattern.compile("");

    /**
     * Returns (or loads) a value from the pattern cache.
     *
     * @param prefix partition used to store similar patterns with different goals
     * @param regex  regex that will be used to compile the pattern, also used as part of the key
     * @param loader used to load the missing value into the cache should it not already exist
     */
    private Pattern get(final String prefix, final String regex, final Callable<Pattern> loader) {
        if (regex == null) {
            return NULL_PATTERN;
        }
        try {
            return CACHE.get(prefix + regex, loader);
        } catch (ExecutionException e) {
            return NULL_PATTERN;
        }
    }

    private String escapeSpecialCharacters(String string) {
        for (final char special : SPECIAL) {
            string = string.replace(String.valueOf(special), "\\" + special);
        }
        return string;
    }

    /**
     * Gets a regex pattern object that will match strings that are exactly the given string
     *
     * @param string a regular string of text
     * @return a Pattern object
     */
    public Pattern getPatternForExactString(final String string) {
        return get(EQ, string, () -> Pattern.compile('^' + escapeSpecialCharacters(string) + '$'));
    }

    /**
     * Gets a regex pattern object that will match strings that are exactly the given string
     *
     * @param string             a regular string of text
     * @param optionalQuantifier whether this string should also match quantified strings e.g. Jug of water (14)
     * @return a Pattern object
     */
    public Pattern getPatternForExactString(final String string, boolean optionalQuantifier) {
        if (!optionalQuantifier) {
            return getPatternForExactString(string);
        }
        return get(EQ, string, () -> Pattern.compile('^' + escapeSpecialCharacters(string) + "( \\(\\d+\\))?" + '$'));
    }

    /**
     * Gets a regex pattern object that will match strings that start with the given string
     *
     * @param string a regular string of text
     * @return a Pattern object
     */
    public Pattern getPatternForStartsWith(final String string) {
        return get(STARTS, string, () -> Pattern.compile('^' + escapeSpecialCharacters(string)));
    }

    /**
     * Gets a regex pattern object that will match strings that end with the given string
     *
     * @param string a regular string of text
     * @return a Pattern object
     */
    public Pattern getPatternForEndsWith(final String string) {
        return get(ENDS, string, () -> Pattern.compile(escapeSpecialCharacters(string) + '$'));
    }

    /**
     * Gets a regex pattern object that will match strings that contains the given string
     *
     * @param string a regular string of text
     * @return a Pattern object
     */
    public Pattern getPatternForContainsString(final String string) {
        return get(CONTAINS, string, () -> Pattern.compile(".*" + escapeSpecialCharacters(string) + ".*"));
    }


    /**
     * Gets a regex pattern object that will match strings that equal any of the given strings
     *
     * @param strings one or more regular strings of text
     * @return a Pattern object
     */
    public Pattern getPatternForExactStrings(final String... strings) {
        final var joiner = new StringJoiner("|", "^", "$");
        for (final var string : strings) {
            joiner.add(escapeSpecialCharacters(string));
        }
        return get(MULTI, joiner.toString(), () -> Pattern.compile(joiner.toString()));
    }

    /**
     * Gets a regex pattern object that will match strings that contain any of the given strings
     *
     * @param strings one or more regular strings of text
     * @return a Pattern object
     */
    public Pattern getPatternContainingOneOf(final String... strings) {
        final var joiner = new StringJoiner("|", "^", "$");
        for (final var string : strings) {
            joiner.add(".*" + escapeSpecialCharacters(string) + ".*");
        }
        return get(MULTI, joiner.toString(), () -> Pattern.compile(joiner.toString()));
    }

    public List<Pattern> getPatternsForExactStrings(final String... strings) {
        List<Pattern> list = new ArrayList<>(strings.length);
        for (String string : strings) {
            list.add(getPatternForExactString(string));
        }
        return list;
    }

    public List<Pattern> getPatternsForContainsStrings(final String... strings) {
        List<Pattern> list = new ArrayList<>(strings.length);
        for (String string : strings) {
            list.add(getPatternForContainsString(string));
        }
        return list;
    }

    /**
     * Compiles the supplied regex into a {@link java.util.regex.Pattern} and then caches it util it's needed next (compiling regex is slow).
     *
     * @param regex a valid regex pattern
     * @return a Pattern compiled from the regex or retrieved from the cache of previously requested Patterns.
     */
    public Pattern getPattern(String regex) {
        return get("", regex, () -> Pattern.compile(regex));
    }
}
