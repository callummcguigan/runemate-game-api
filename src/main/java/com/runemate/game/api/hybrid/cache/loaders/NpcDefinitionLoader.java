package com.runemate.game.api.hybrid.cache.loaders;

import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.cache.item.*;
import java.util.*;

public class NpcDefinitionLoader extends SerializedFileLoader<CacheNpcDefinition> {

    public NpcDefinitionLoader(int file) {
        super(file);
    }

    @Override
    protected CacheNpcDefinition construct(int entry, int file, Map<String, Object> arguments) {
        return new CacheNpcDefinition(file);
    }
}
