package com.runemate.game.api.hybrid.cache.elements;

import com.runemate.game.api.hybrid.cache.configs.*;
import com.runemate.game.api.hybrid.cache.materials.*;
import com.runemate.game.cache.io.*;
import com.runemate.game.cache.item.*;
import java.io.*;

public class CacheUnderlayDefinition extends IncrementallyDecodedItem
    implements UnderlayDefinition {
    public int id;
    public int materialId = -1;
    public int rgb_color;
    public boolean block_shadow = true;
    public int hue;
    public int saturation;
    public int lightness;
    public int chroma;
    public boolean aBoolean2239 = true;
    public int scale = 512;

    public CacheUnderlayDefinition(int id) {
        this.id = id;
    }

    @Override
    protected void decode(Js5InputStream stream, int opcode) throws IOException {
        if (opcode == 1) {
            rgb_color = stream.readBytes(3);
            setHSL(rgb_color);
        } else if (opcode == 2) {
            materialId = stream.readUnsignedShort();
            if (materialId == 65535) {
                materialId = -1;
            }
        } else if (opcode == 3) {
            scale = stream.readUnsignedShort() << 2;
        } else if (opcode == 4) {
            block_shadow = false;
        } else if (opcode == 5) {
            aBoolean2239 = false;
        }
    }

    private void setHSL(int color) {
        double red = (color >> 16 & 0xff) / 256.0;
        double green = (color >> 8 & 0xff) / 256.0;
        double blue = (color & 0xff) / 256.0;
        double minimum_color = red;
        if (green < minimum_color) {
            minimum_color = green;
        }
        if (blue < minimum_color) {
            minimum_color = blue;
        }
        double maximum_color = red;
        if (green > maximum_color) {
            maximum_color = green;
        }
        if (blue > maximum_color) {
            maximum_color = blue;
        }
        double h = 0.0;
        double s = 0.0;
        double l = (minimum_color + maximum_color) / 2.0;
        if (minimum_color != maximum_color) {
            if (l < 0.5) {
                s = (maximum_color - minimum_color) / (minimum_color + maximum_color);
            }
            if (red == maximum_color) {
                h = (green - blue) / (maximum_color - minimum_color);
            } else if (green == maximum_color) {
                h = (blue - red) / (maximum_color - minimum_color) + 2.0;
            } else if (blue == maximum_color) {
                h = (red - green) / (maximum_color - minimum_color) + 4.0;
            }
            if (l >= 0.5) {
                s = (maximum_color - minimum_color) / (2.0 - maximum_color - minimum_color);
            }
        }
        h /= 6.0;
        lightness = (int) (l * 256.0);
        saturation = (int) (s * 256.0);
        if (l > 0.5) {
            chroma = (int) ((1.0 - l) * s * 512.0);
        } else {
            chroma = (int) (l * s * 512.0);
        }
        if (saturation < 0) {
            saturation = 0;
        } else if (saturation > 255) {
            saturation = 255;
        }
        if (lightness < 0) {
            lightness = 0;
        } else if (lightness > 255) {
            lightness = 255;
        }
        if (chroma < 1) {
            chroma = 1;
        }
        hue = (int) (h * chroma);
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public Material getMaterial() {
        if (materialId < 0) {
            return null;
        }
        return Materials.load(materialId);
    }

    @Override
    public String toString() {
        return "UnderlayDefinition{id=" + id + '}';
    }
}
