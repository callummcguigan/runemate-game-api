package com.runemate.game.api.hybrid.web;

import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.calculations.*;
import com.runemate.game.api.hybrid.web.vertex.*;
import com.runemate.game.internal.*;
import java.util.*;
import lombok.*;
import lombok.experimental.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

@Log4j2
@FieldNameConstants(innerTypeName = "Cache")
@RequiredArgsConstructor
public class WebPath extends Path {

    public static final @InternalAPI String PREVIOUS = "PREVIOUS";
    public static final @InternalAPI String STEPS = "STEPS";
    public static final @InternalAPI String AVATAR = "AVATAR";
    public static final @InternalAPI String REGION = "REGION";
    public static final @InternalAPI String REACHABLE = "REACHABLE";
    public static final @InternalAPI String REGION_BASE = "REGION_BASE";
    public static final @InternalAPI String AVATAR_POS = "AVATAR_POS";
    public static final @InternalAPI String NPC_CACHE = "NPC_CACHE";
    public static final @InternalAPI String ITEM_CACHE = "ITEM_CACHE";
    public static final @InternalAPI String PREFERS_VIEWPORT = "PREFERS_VIEWPORT";

    private final List<Vertex> vertices;
    private Vertex current;
    private int lastSuccessfulIndex;

    private final Map<String, Object> cache = new HashMap<>();

    @Override
    public List<? extends Locatable> getVertices() {
        return vertices;
    }

    @Nullable
    @Override
    public Vertex getNext() {
        return getNext(false);
    }

    @Override
    public Vertex getNext(final boolean preferViewportTraversal) {
        if (vertices.isEmpty()) {
            return null;
        }

        final var region = Region.getArea();
        final var player = Players.getLocal();
        final Coordinate playerPos;
        if (player == null || (playerPos = player.getPosition()) == null) {
            return null;
        }

        final int[][] flags = Region.getCollisionFlags(region.getPosition().getPlane());
        if (flags == null) {
            log.warn("Failed to find next step since the region flags could not be resolved");
            return null;
        }

        final var reachable = playerPos.getReachableCoordinates(flags);
        if (reachable.isEmpty()) {
            log.warn("Failed to find next step since there are no reachable neighbours from {}", playerPos);
            return null;
        }

        cache.clear();
        cache.put(AVATAR, player);
        cache.put(AVATAR_POS, playerPos);
        cache.put(REGION, region);
        cache.put(REGION_BASE, region.getBottomLeft());
        cache.put(REACHABLE, reachable);
        cache.put(NPC_CACHE, Npcs.getLoaded());
        cache.put(ITEM_CACHE, Inventory.getItems());
        cache.put(PREFERS_VIEWPORT, preferViewportTraversal);

        Vertex suggested = null;
        TeleportVertex teleport = null;

        var index = lastSuccessfulIndex;
        for (; index < vertices.size(); index++) {
            final var current = vertices.get(index);
            final var previous = index > 0 ? vertices.get(index - 1) : null;
            cache.put(PREVIOUS, previous);

            final var result = current.scan(cache);
            var candidate = result.getVertex();
            if (candidate != null) {
                if (candidate instanceof TeleportVertex) {
                    teleport = (TeleportVertex) candidate;
                } else {
                    suggested = candidate;
                }
            }
            if (ScanAction.STOP == result.getAction()) {
                break;
            }
        }

        Locatable last;
        if (suggested == null && (last = getLast()) != null && !playerPos.equals(last.getPosition())) {
            suggested = teleport;
        }

        current = suggested;
        log.debug("Next step is {}, #{} in the path at distance {}", suggested, index, Distance.between(player, current));
        return suggested;
    }

    @Override
    public boolean step(@NotNull TraversalOption... options) {
        boolean toggleRunBeforeStepping = PlayerSense.getAsBoolean(PlayerSense.Key.TOGGLE_RUN_BEFORE_TRAVERSING);
        if (toggleRunBeforeStepping && !triggerRun(options)) {
            log.debug("Failed to enable running");
            return false;
        }

        if (!isEligibleToStep(options)) {
            return triggerStaminaEnhancement(options) && (toggleRunBeforeStepping || triggerRun(options));
        }

        final var next = getNext();
        if (next == null) {
            if (lastSuccessfulIndex > 0) {
                log.warn("Failed to resolve next vertex after analyzing {} candidates, resetting scan index", lastSuccessfulIndex);
                reset();
                return step(options);
            } else {
                log.warn("Failed to resolve next step - new path required");
                return false;
            }
        }

        current = null;
        var stepped = next.step(cache);

        if (!stepped) {
            return false;
        }

        if (!(next instanceof BasicVertex.Fake)) {
            var lastSuccess = getVertices().indexOf(next);
            if (lastSuccess == -1) {
                log.warn("{} is missing in the vertex list", next);
                return true;
            }

            log.debug("Updating last successful index to {}", lastSuccess);
            lastSuccessfulIndex = lastSuccess;
        }

        return true;
    }

    public Vertex getLast() {
        return vertices.isEmpty() ? null : vertices.get(vertices.size() - 1);
    }

    public void reset() {
        current = null;
        lastSuccessfulIndex = 0;
    }

    public static WebPath buildTo(Landmark destination) {
        final var start = Players.getLocal();
        return WebPathRequest.builder()
            .setStart(start)
            .setLandmark(destination)
            .build();
    }

    public static WebPath buildTo(Locatable destination) {
        return buildBetween(Players.getLocal(), destination);
    }

    public static WebPath buildBetween(Locatable start, Locatable destination) {
        return WebPathRequest.builder()
            .setStart(start)
            .setDestination(destination)
            .build();
    }
}
