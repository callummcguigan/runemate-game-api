package com.runemate.game.api.hybrid.web.vertex;

import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.util.calculations.*;
import com.runemate.game.api.hybrid.web.*;
import com.runemate.game.internal.*;
import java.util.*;
import lombok.*;
import org.jetbrains.annotations.*;

@ToString
@RequiredArgsConstructor
public class BasicVertex implements Vertex {

    protected final @NonNull Coordinate position;

    @NonNull
    @Override
    public Coordinate getPosition() {
        return position;
    }

    @Nullable
    @Override
    public Coordinate.HighPrecision getHighPrecisionPosition() {
        return position.getHighPrecisionPosition();
    }

    @Nullable
    @Override
    public Area.Rectangular getArea() {
        return position.getArea();
    }

    @Override
    public boolean step(final Map<String, Object> cache) {
        final var viewport = (boolean) cache.get(WebPath.PREFERS_VIEWPORT);
        if (viewport) {
            return position.interact("Walk here");
        }
        return position.minimap().click();
    }

    @Override
    @SuppressWarnings("unchecked")
    public ScanResult scan(final Map<String, Object> cache) {
        final var reachable = (Set<Coordinate>) cache.get(WebPath.REACHABLE);
        final var player = (Locatable) cache.get(WebPath.AVATAR_POS);
        if (Distance.between(position, player) == 0 || !reachable.contains(position)) {
            return new ScanResult(null, ScanAction.CONTINUE);
        }

        final var viewport = (boolean) cache.get(WebPath.PREFERS_VIEWPORT);
        if (viewport) {
            return new ScanResult(position.isVisible() ? this : null, ScanAction.CONTINUE);
        }

        return new ScanResult(position.minimap().isVisible() ? this : null, ScanAction.CONTINUE);
    }

    @InternalAPI
    public static class Fake extends BasicVertex {

        public Fake(@NonNull final Coordinate position) {
            super(position);
        }
    }
}
