package com.runemate.game.api.hybrid.cache.loaders;

import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.cache.item.*;
import java.util.*;

public class ParameterDefinitionLoader extends SerializedFileLoader<CacheParameterDefinition> {
    private final boolean osrs;

    public ParameterDefinitionLoader(int file, boolean osrs) {
        super(file);
        this.osrs = osrs;
    }

    @Override
    protected CacheParameterDefinition construct(int entry, int file, Map<String, Object> arguments) {
        return new CacheParameterDefinition(osrs ? file : entry, osrs);
    }
}