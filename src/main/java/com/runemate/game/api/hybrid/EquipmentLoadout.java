package com.runemate.game.api.hybrid;

import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.google.common.cache.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import org.jetbrains.annotations.*;

public class EquipmentLoadout extends EnumMap<Equipment.Slot, Pattern> {

    private static final Cache<String, ItemDefinition> CACHE = CacheBuilder.newBuilder().expireAfterAccess(5, TimeUnit.MINUTES).build();

    public EquipmentLoadout() {
        super(Equipment.Slot.class);
    }

    public @Nullable ItemDefinition getDefinition(Equipment.Slot key) {
        final var pattern = get(key);
        if (pattern == null) {
            return null;
        }
        var definition = CACHE.getIfPresent(pattern.pattern());
        if (definition == null) {
            final var candidates = ItemDefinition.loadAll(named(pattern));
            definition = candidates.isEmpty() ? null : candidates.get(0);
            if (definition != null) {
                CACHE.put(pattern.pattern(), definition);
            }
        }
        return definition;
    }

    /**
     * @return the slots that do not contain items that are part of the EquipmentLoadout
     */
    public EnumSet<Equipment.Slot> getMissingSlots() {
        var result = EnumSet.noneOf(Equipment.Slot.class);
        for (var entry : entrySet()) {
            final var equipped = Equipment.getItemIn(entry.getKey());
            if (equipped == null || !named(entry.getValue()).test(equipped.getDefinition())) {
                result.add(entry.getKey());
            }
        }
        return result;
    }

    private Predicate<ItemDefinition> named(Pattern pattern) {
        return definition -> {
            if (definition == null) {
                return false;
            }
            final var item = definition.getName();
            return pattern.matcher(item).matches();
        };
    }
}
