package com.runemate.game.api.hybrid.queries;

import com.runemate.game.api.hybrid.cache.configs.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.region.*;
import java.util.*;
import java.util.concurrent.*;

public class SpotAnimationQueryBuilder
    extends LocatableEntityQueryBuilder<SpotAnimation, SpotAnimationQueryBuilder> {
    private int[] ids, animationIds, modelIds;

    @Override
    public SpotAnimationQueryBuilder get() {
        return this;
    }

    @Override
    public Callable<List<? extends SpotAnimation>> getDefaultProvider() {
        return () -> SpotAnimations.getLoaded().asList();
    }

    public SpotAnimationQueryBuilder ids(final int... ids) {
        this.ids = ids;
        return get();
    }

    public SpotAnimationQueryBuilder animations(final int... animationIds) {
        this.animationIds = animationIds;
        return get();
    }

    public SpotAnimationQueryBuilder models(final int... modelIds) {
        this.modelIds = modelIds;
        return get();
    }

    @Override
    public boolean accepts(SpotAnimation argument) {
        boolean condition;
        SpotAnimationDefinition definition = null;
        if (ids != null) {
            condition = false;
            int id = argument.getId();
            for (final int value : this.ids) {
                if (value == id) {
                    condition = true;
                    break;
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (animationIds != null) {
            condition = false;
            int animationId = argument.getAnimationId();
            for (final int value : this.animationIds) {
                if (value == animationId) {
                    condition = true;
                    break;
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (modelIds != null) {
            if (definition == null) {
                definition = argument.getDefinition();
            }
            condition = false;
            if (definition != null) {
                int modelId = definition.getModelId();
                for (final int value : this.modelIds) {
                    if (value == modelId) {
                        condition = true;
                        break;
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        return super.accepts(argument);
    }
}
