package com.runemate.game.api.rs3.local.hud.interfaces.legacy;

import com.runemate.game.api.hybrid.local.hud.interfaces.*;

@Deprecated
public enum LegacyTab implements Tab, Closeable, Lockable {
    COMBAT_SETTINGS,
    @Deprecated ACTIVE_TASK,
    ACTIVITY_TRACKER,
    SKILLS,
    @Deprecated ADVENTURES,
    QUEST_LIST,
    BACKPACK,
    WORN_EQUIPMENT,
    PRAYER,
    MAGIC_BOOK,
    EXTRAS,
    FRIENDS,
    FRIENDS_CHAT_INFO,
    CLAN,
    SETTINGS,
    EMOTES,
    MUSIC_PLAYER,
    MINIGAMES,
    GROUP_MEMBERS,
    @Deprecated NOTES;

    LegacyTab() {
        // no-op
    }

    public static LegacyTab getOpened() {
        return null;
    }

    @Override
    public boolean close() {
        return false;
    }

    public boolean close(final boolean forceClick) {
        return false;
    }

    @Override
    @Deprecated
    public boolean isClosed() {
        return false;
    }

    @Override
    public boolean open() {
        return false;
    }

    @Override
    public boolean isOpen() {
        return false;
    }

    public boolean open(final boolean forceClick) {
        return false;
    }

    @Override
    public boolean isLocked() {
        return false;
    }

    public InterfaceComponent getComponent() {
        return null;
    }

    @Override
    public String toString() {
        return "LegacyTab." + name();
    }
}
