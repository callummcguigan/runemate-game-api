package com.runemate.ui.control;

import com.google.common.base.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.script.framework.core.*;
import com.runemate.ui.converter.*;
import com.runemate.ui.setting.descriptor.open.*;
import com.runemate.ui.setting.open.*;
import javafx.collections.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.util.*;
import lombok.experimental.*;
import lombok.extern.log4j.*;
import org.apache.commons.text.*;

/**
 * Utility class for producing JavaFX controls for settings. This can be useful if the author wants to create
 * their own implementation of the settings UI rather than using the default implementation.
 */
@Log4j2
@UtilityClass
public class SettingControlFactory {

    public CheckBox createCheckbox(SettingsManager manager, SettingsDescriptor group, SettingDescriptor setting) {
        final var checkbox = new CheckBox();
        applyNodeDefaults(checkbox, manager, setting);

        checkbox.setSelected(Boolean.parseBoolean(manager.get(group, setting)));
        checkbox.selectedProperty().addListener((obs, oldValue, newValue) -> changeConfiguration(manager, group, setting, checkbox));
        return checkbox;
    }

    public TextField createTextField(SettingsManager manager, SettingsDescriptor group, SettingDescriptor setting) {
        final var field = setting.setting().secret() ? new PasswordField() : new TextField();
        applyNodeDefaults(field, manager, setting);

        field.setText(manager.get(group, setting));
        //We use setOnAction rather than listening to the textProperty() because we don't want I/O every time the user makes a keystroke
        field.setOnAction(e -> changeConfiguration(manager, group, setting, field));

        //Also send a change when the field loses focus
        field.focusedProperty().addListener((obs, oldValue, newValue) -> {
            if (oldValue && !newValue) {
                changeConfiguration(manager, group, setting, field);
            }
        });
        return field;
    }

    public Spinner<Integer> createIntSpinner(SettingsManager manager, SettingsDescriptor group, SettingDescriptor setting) {
        final var spinner = new Spinner<Integer>();
        applyNodeDefaults(spinner, manager, setting);
        spinner.setEditable(true);

        final var range = setting.range();
        final var initialStr = manager.get(group, setting);
        final var initial = Strings.isNullOrEmpty(initialStr) ? 0 : Integer.parseInt(initialStr);
        final var factory = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE);

        if (setting.suffix() != null) {
            factory.setConverter(new SuffixConverter(setting.suffix()));
        }

        if (range != null) {
            factory.setMin(range.min());
            factory.setMax(range.max());
            factory.setAmountToStepBy(range.step());

            //Apply constraint in case previous value is outside of updated range
            factory.setValue(Math.min(Math.max(initial, range.min()), range.max()));
        } else {
            factory.setValue(initial);
        }

        spinner.setValueFactory(factory);
        spinner.valueProperty().addListener((obs, oldValue, newValue) -> changeConfiguration(manager, group, setting, spinner));
        return spinner;
    }

    public Spinner<Double> createDoubleSpinner(SettingsManager manager, SettingsDescriptor group, SettingDescriptor setting) {
        final var spinner = new Spinner<Double>();
        applyNodeDefaults(spinner, manager, setting);
        spinner.setEditable(true);

        final var initial = Double.parseDouble(manager.get(group, setting));
        spinner.setValueFactory(new SpinnerValueFactory.DoubleSpinnerValueFactory(0, Double.MAX_VALUE, initial, 0.1));
        spinner.valueProperty().addListener((obs, oldValue, newValue) -> changeConfiguration(manager, group, setting, spinner));
        return spinner;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public ComboBox<?> createEnumComboBox(SettingsManager manager, SettingsDescriptor group, SettingDescriptor setting) {
        final var type = (Class<? extends Enum>) setting.type();
        final var cb = new ComboBox<Enum<?>>();
        applyNodeDefaults(cb, manager, setting);

        cb.setMaxWidth(Double.MAX_VALUE);
        cb.setItems(FXCollections.observableArrayList(type.getEnumConstants()));
        cb.setConverter(new EnumConverter());
        final var initialText = manager.get(group, setting);

        try {
            final var initial = Enum.valueOf(type, initialText);
            if (cb.getItems().contains(initial)) {
                cb.getSelectionModel().select(initial);
            }
        } catch (NullPointerException e) {
            log.warn("No enum constant named '" + initialText + "' found");
        }

        cb.getSelectionModel().selectedItemProperty().addListener((obs, old, value) -> changeConfiguration(manager, group, setting, cb));
        return cb;
    }

    public Button createPlayerPositionButton(BotPlatform bp, SettingsManager manager, SettingsDescriptor group, SettingDescriptor setting) {
        final var button = new ValueButton<>(() -> {
            try {
                return bp.invokeAndWait(() -> Players.getLocal().getPosition());
            } catch (Exception e) {
                return null;
            }
        });
        applyNodeDefaults(button, manager, setting);

        final var initialText = manager.get(group, setting);
        if (!Strings.isNullOrEmpty(initialText)) {
            button.setValue(CoordinateSettingConverter.fromString(initialText));
        }

        button.valueProperty().addListener((obs, oldValue, newValue) -> changeConfiguration(manager, group, setting, button));
        return button;
    }

    public EquipmentButton createEquipmentButton(BotPlatform bp, SettingsManager manager, SettingsDescriptor group, SettingDescriptor setting) {
        final var button = new EquipmentButton(bp, manager, group, setting);
        applyNodeDefaults(button, manager, setting);
        return button;
    }

    private void applyNodeDefaults(Node node, SettingsManager manager, SettingDescriptor setting) {
        node.managedProperty().bind(node.visibleProperty());
        node.setDisable(setting.setting().disabled());
        node.setFocusTraversable(true);
        node.disableProperty().bind(manager.lockedProperty());
    }

    private void changeConfiguration(SettingsManager manager, SettingsDescriptor group, SettingDescriptor setting, Control control) {
        //TODO warning/confirmation popup for applicable controls
        if (control instanceof CheckBox) {
            final var cb = (CheckBox) control;
            manager.set(group, setting, String.valueOf(cb.isSelected()));
        } else if (control instanceof TextField) {
            final var tf = (TextField) control;
            if (Strings.isNullOrEmpty(tf.getText())) {
                manager.remove(group, setting);
            } else {
                manager.set(group, setting, tf.getText());
            }
        } else if (control instanceof Spinner) {
            manager.set(group, setting, String.valueOf(((Spinner<?>) control).getValue()));
        } else if (control instanceof ComboBox<?>) {
            manager.set(group, setting, ((ComboBox<Enum>) control).getValue().name());
        } else if (control instanceof ValueButton) {
            final var value = ((ValueButton) control).getValueString();
            if (value != null) {
                manager.set(group, setting, value);
            } else {
                manager.remove(group, setting);
            }
        }
    }

    private static class EnumConverter extends StringConverter<Enum<?>> {

        @Override
        public String toString(final Enum<?> o) {
            String toString = o.toString();

            //base toString() impl returns name()
            if (o.name().equals(toString)) {
                return WordUtils.capitalize(toString.toLowerCase(), '_').replace("_", " ");
            }

            return toString;
        }

        @Override
        public Enum<?> fromString(final String string) {
            return null;
        }
    }
}
