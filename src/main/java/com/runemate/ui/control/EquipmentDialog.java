package com.runemate.ui.control;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.script.framework.core.*;
import java.util.*;
import javafx.geometry.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import lombok.*;
import lombok.extern.log4j.*;

@Log4j2
public class EquipmentDialog extends Dialog<EquipmentLoadout> {

    public EquipmentDialog(final BotPlatform bot) {
        this(bot, new EquipmentLoadout());
    }

    public EquipmentDialog(final BotPlatform bot, @NonNull EquipmentLoadout existing) {
        setTitle("Equipment selector");

        Map<Equipment.Slot, SlotControl> slots = new EnumMap<>(Equipment.Slot.class);
        for (var slot : Equipment.Slot.values()) {
            try {
                var initial = bot.invokeAndWait(() -> existing.getDefinition(slot));
                if (initial != null) {
                    slots.put(slot, new SlotControl(bot, slot, initial));
                } else {
                    slots.put(slot, new SlotControl(bot, slot));
                }
            } catch (Exception e) {
                log.warn("Failed to look up existing ItemDefinition", e);
            }
        }

        var grid = new GridPane();
        var hConstraints = new ColumnConstraints(36, 36, 36);
        var vConstraints = new RowConstraints(36, 36, 36);
        grid.setVgap(1);
        grid.setHgap(1);
        grid.getColumnConstraints().add(hConstraints);
        grid.getRowConstraints().add(vConstraints);
        grid.setPadding(new Insets(4));
        grid.add(slots.get(Equipment.Slot.HEAD), 1, 0);
        grid.add(slots.get(Equipment.Slot.CAPE), 0, 1);
        grid.add(slots.get(Equipment.Slot.NECK), 1, 1);
        grid.add(slots.get(Equipment.Slot.AMMUNITION), 2, 1);
        grid.add(slots.get(Equipment.Slot.WEAPON), 0, 2);
        grid.add(slots.get(Equipment.Slot.BODY), 1, 2);
        grid.add(slots.get(Equipment.Slot.SHIELD), 2, 2);
        grid.add(slots.get(Equipment.Slot.LEGS), 1, 3);
        grid.add(slots.get(Equipment.Slot.HANDS), 0, 4);
        grid.add(slots.get(Equipment.Slot.FEET), 1, 4);
        grid.add(slots.get(Equipment.Slot.RING), 2, 4);

        var content = new VBox(4.0);
        content.getChildren().add(grid);

        final var refresh = new Button("Refresh");
        final var clear = new Button("Clear");

        refresh.setPrefWidth(110);
        clear.setPrefWidth(110);

        refresh.setOnAction(e -> slots.values().forEach(SlotControl::refresh));
        clear.setOnAction(e -> slots.values().forEach(SlotControl::clear));

        content.getChildren().addAll(refresh, clear);

        final var ok = new ButtonType("Save", ButtonBar.ButtonData.OK_DONE);
        getDialogPane().getButtonTypes().add(ok);
        getDialogPane().setContent(content);
        setResultConverter(button -> {
            if (ok.equals(button)) {
                return null;
            }

            var results = new EquipmentLoadout();
            for (var slot : slots.entrySet()) {
                results.put(slot.getKey(), slot.getValue().getPattern());
            }
            return results;
        });
    }
}
