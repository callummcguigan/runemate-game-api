package com.runemate.ui.control;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.script.framework.core.*;
import com.runemate.ui.setting.descriptor.open.*;
import com.runemate.ui.setting.open.*;
import java.util.regex.*;
import javafx.beans.property.*;
import javafx.scene.control.*;
import lombok.extern.log4j.*;

@Log4j2
public class EquipmentButton extends Button {

    private final ObjectProperty<EquipmentLoadout> valueProperty;

    public EquipmentButton(BotPlatform bot, SettingsManager manager, SettingsDescriptor group, SettingDescriptor setting) {
        setText("Equipment");
        valueProperty = new SimpleObjectProperty<>();

        var map = new EquipmentLoadout();
        for (var slot : Equipment.Slot.values()) {
            var existing = manager.get(group.group().group(), key(setting, slot));
            if (existing != null && !existing.isEmpty()) {
                map.put(slot, Pattern.compile(existing));
            }
        }
        valueProperty.set(map);
        valueProperty.addListener((obs, old, value) -> {
            for (var slot : Equipment.Slot.values()) {
                manager.remove(group.group().group(), key(setting, slot));
            }

            var i = 0;
            for (var slot : value.entrySet()) {
                if (slot.getValue() != null) {
                    log.trace("Setting {}.{} to {}", group.group().group(), key(setting, slot.getKey()), slot.getValue().pattern());
                    manager.set(group.group().group(), key(setting, slot.getKey()), slot.getValue().pattern());
                    i++;
                }
            }

            if (i > 0) {
                setText(i + " items");
            } else {
                setText("Not set");
            }
        });

        setOnAction(event -> {
            setText("Loading...");
            final var dialog = new EquipmentDialog(bot, valueProperty.get());
            final var value = dialog.showAndWait();
            value.ifPresentOrElse(valueProperty::set, () -> valueProperty.set(new EquipmentLoadout()));
        });
    }

    private static String key(SettingDescriptor setting, Equipment.Slot slot) {
        return setting.setting().key() + "." + slot.name();
    }
}
