package com.runemate.ui.control;

import com.runemate.ui.setting.open.*;
import java.util.function.*;
import javafx.beans.property.*;
import javafx.scene.control.*;
import javafx.scene.input.*;
import javafx.util.*;
import lombok.*;
import lombok.experimental.*;
import org.jetbrains.annotations.*;

@Accessors(fluent = true)
public class ValueButton<T> extends Button {

    @Getter
    private final ObjectProperty<T> valueProperty = new SimpleObjectProperty<>();

    @Getter
    private final ObjectProperty<SettingConverter> settingConverterProperty = new SimpleObjectProperty<>();

    public ValueButton(@NonNull Supplier<T> provider, @Nullable final StringConverter<T> converter) {
        setText("Not set");
        setTooltip(new Tooltip("Left-click to update, right-click to reset."));
        valueProperty().addListener((obs, oldValue, newValue) -> {
            if (newValue == null) {
                setText("Not set");
            } else {
                setText(converter == null ? newValue.toString() : converter.toString(newValue));
            }
        });
        setOnMouseClicked(event -> {
            if (event.getButton() == MouseButton.SECONDARY) {
                setValue(null);
            } else if (event.getButton() == MouseButton.PRIMARY) {
                setValue(provider.get());
            }
        });
    }

    public ValueButton(@NonNull Supplier<T> provider) {
        this(provider, null);
    }

    public void setValue(@Nullable T value) {
        valueProperty().setValue(value);
    }

    @Nullable
    public T getValue() {
        return valueProperty().getValue();
    }

    public void setSettingConverter(@NonNull SettingConverter converter) {
        settingConverterProperty().setValue(converter);
    }

    @Nullable
    public String getValueString() {
        final var value = getValue();
        if (value == null) {
            return null;
        }
        if (value instanceof String) {
            return (String) value;
        }
        final var converter = settingConverterProperty().getValue();
        if (converter == null) {
            return null;
        }
        return converter.toString(value);
    }
}
